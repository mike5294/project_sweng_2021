package com.journal.client;

import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.journal.shared.Article;

@RemoteServiceRelativePath("articles")
public interface ArticleService extends RemoteService {
	
	Article createArticle(String title, String body, String author, String categoryName, boolean premium);
	Set<Article> getArticles();
	Article getArticleById(String idArticle);
	Boolean deleteArticle(String idArticle);
	Article updateArticle(String idArticle, String body, boolean premium, String title, String category);
	
}
