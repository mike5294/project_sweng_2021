package com.journal.client;

import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.journal.shared.Article;

public interface ArticleServiceAsync {
	void createArticle(String title, String body, String author, String categoryName, boolean premium, AsyncCallback<Article> callback);
	
	void getArticles(AsyncCallback<Set<Article>> callback);
	
	void getArticleById(String idArticle, AsyncCallback<Article> callback);
	
	void deleteArticle(String idArticle, AsyncCallback<Boolean> callback);

	void updateArticle(String idArticle, String body, boolean premium, String title, String category, AsyncCallback<Article> callback);

}
