package com.journal.client;

import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.journal.shared.Category;

@RemoteServiceRelativePath("categories")
public interface CategoryService extends RemoteService {
	
	Set<Category> getCategories();
	
	Category createCategory(String name, String parent);
	
	Category updateCategory(String oldName, String newName);
	
	Boolean deleteCategory(String name);
}
