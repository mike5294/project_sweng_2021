package com.journal.client;

import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.journal.shared.Category;

public interface CategoryServiceAsync {
	
	void getCategories(AsyncCallback<Set<Category>> callback);
	
	void createCategory(String name, String parent, AsyncCallback<Category> callback);
	
	void updateCategory(String oldName, String newName, AsyncCallback<Category> callback);
	
	void deleteCategory(String name, AsyncCallback<Boolean> callback);
}
