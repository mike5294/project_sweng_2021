package com.journal.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.journal.shared.Article;

@RemoteServiceRelativePath("comments")
public interface CommentService extends RemoteService{
	
	Article createComment(String articleID, String email, String text);
	
	Article approveComment(String idArticle, String idComment);
	
	Article deleteComment(String idArticle, String idComment);
}
