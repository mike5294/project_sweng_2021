package com.journal.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.journal.shared.Article;


public interface CommentServiceAsync {
	void createComment(String articleID, String email, String text, AsyncCallback<Article> callback);
	void approveComment(String idArticle, String idComment, AsyncCallback<Article> callback);
	void deleteComment(String idArticle, String idComment, AsyncCallback<Article> callback);
}
