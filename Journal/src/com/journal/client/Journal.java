package com.journal.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.journal.shared.Article;
import com.journal.shared.Category;
import com.journal.shared.Comment;
import com.journal.shared.CreditCard;
import com.journal.shared.Like;
import com.journal.shared.User;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Journal implements EntryPoint {
	private static final String ROOT_PANEL = "stockList";

	private ArticleServiceAsync articleService = GWT.create(ArticleService.class);
	private UserServiceAsync userService = GWT.create(UserService.class);
	private CommentServiceAsync commentService = GWT.create(CommentService.class);
	private LikeServiceAsync likeService = GWT.create(LikeService.class);
	private CategoryServiceAsync categoryService = GWT.create(CategoryService.class);

	private User user;
	private User author;

	private FlexTable articlesTable;
	private FlexTable categoriesTable;
	private VerticalPanel articlePanel;
	private VerticalPanel categoryPanel;
	private VerticalPanel categoriesPanel;
	private VerticalPanel saveArticlePanel;

	public void onModuleLoad() {
		Widget articlesPanel = createArticlesPanel();
		RootPanel.get(ROOT_PANEL).add(articlesPanel);
	}

	//Metodo di gestione Widget a pannello Root
	private void switchWidget(Widget widget) {
		RootPanel.get(ROOT_PANEL).add(widget);
		RootPanel.get(ROOT_PANEL).remove(0);
	}
	
	//Metodo di aggiunta Widget al pannello radice
	private void addWidgetsToPanel(Panel panel, Widget... widgets) {
		for (int i = 0; i < widgets.length; i++) {
			panel.add(widgets[i]);
		}
	}

	/* ------------------------------------------------------------------------------ */

	//Creazione pannello di login
	private Widget createLoginPanel() {

		final VerticalPanel loginPanel = new VerticalPanel();
		final TextBox emailField = new TextBox();
		final PasswordTextBox passwordField = new PasswordTextBox();

		final Button loginButton = new Button("Login");
		loginButton.setStyleName("buttonLogin");
		final Button signUpButton = new Button("Create an account");
		signUpButton.setStyleName("buttonLogin");

		emailField.getElement().setAttribute("placeholder", "email");
		emailField.setStyleName("textBoxLogin");
		passwordField.getElement().setAttribute("placeholder", "password");
		passwordField.setStyleName("textBoxLogin");

		loginPanel.add(emailField);
		loginPanel.add(passwordField);
		loginPanel.add(loginButton);
		loginPanel.add(signUpButton);

		loginPanel.setStyleName("loginPanel");
		loginButton.addClickHandler(event -> handleLoginButton(emailField.getText(), passwordField.getText()));
		signUpButton.addClickHandler(event -> {
			Widget signUpPanel = createSignUpPanel();
			switchWidget(signUpPanel);
		});

		return loginPanel;
	}

	//Metodo Handle per gestire il login dell' utente che intende effettuare l' accesso
	private void handleLoginButton(String email, String password) {        
		userService.getUser(email, password, new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Login failed");
			}

			@Override
			public void onSuccess(User result) {
				if (Objects.nonNull(result)) {
					handleLoginSuccessful(result);						
				} else {
					Window.alert("Login failed");
				}

			}
		});
	}

	//Metodo handle del login avvenuto con successo
	private void handleLoginSuccessful(User user) {
		this.user = user;
		Widget articlesPanel = createArticlesPanel();
		switchWidget(articlesPanel);

		Widget createArticlePanel = createArticlePanel(null);
		this.saveArticlePanel.clear();
		this.saveArticlePanel.add(createArticlePanel);
	}

	/* ------------------------------------------------------------------------------ */

	//Creazione del pannello di Sign up
	private Widget createSignUpPanel() {

		final VerticalPanel signUpPanel = new VerticalPanel();
		signUpPanel.setStyleName("loginPanel");
		final Label emailLabel = new Label("Email:");
		final TextBox emailField = new TextBox();

		final Label passwordLabel = new Label("Password:");
		final PasswordTextBox passwordField = new PasswordTextBox();
		final PasswordTextBox repeatPasswordField = new PasswordTextBox();

		final Label nameLabel = new Label("Name:");
		final TextBox nameField = new TextBox();

		final Label surnameLabel = new Label("Surname:");
		final TextBox surnameField = new TextBox();

		final Label usernameLabel = new Label("Username:");
		final TextBox usernameField = new TextBox();

		final Label bithdateLabel = new Label("Birth date:");
		final DateBox birthdateField = new DateBox();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		birthdateField.setFormat(new DateBox.DefaultFormat(dateFormat));

		final Label addressLabel = new Label("Address:");
		final TextBox addressField = new TextBox();

		final Label birthplaceLabel = new Label("Birth place:");
		final TextBox birthplaceField = new TextBox();

		final Label sexLabel = new Label("Sex:");
		final RadioButton maleSexField = new RadioButton("sexGroup","M");
		final RadioButton femaleSexField = new RadioButton("sexGroup","F");

		FlowPanel panel = new FlowPanel();
		panel.add(maleSexField);
		panel.add(femaleSexField);

		emailField.getElement().setAttribute("placeholder", "* Email");
		passwordField.getElement().setAttribute("placeholder", "* Password");
		repeatPasswordField.getElement().setAttribute("placeholder", "* Repeat Password");
		nameField.getElement().setAttribute("placeholder", "* Name");
		surnameField.getElement().setAttribute("placeholder", "* Surname");
		usernameField.getElement().setAttribute("placeholder", "* Username");
		birthdateField.getElement().setAttribute("placeholder", "* DD/MM/YYYY");
		addressField.getElement().setAttribute("placeholder", "* Street, Town, Country");
		birthplaceField.getElement().setAttribute("placeholder", "District");

		final Button signUpButton = new Button("Sign up");
		final Button loginButton = new Button("Go back to login");

		addWidgetsToPanel(signUpPanel, emailLabel, emailField, passwordLabel, passwordField, repeatPasswordField, 
				nameLabel, nameField, surnameLabel, surnameField, usernameLabel, usernameField, bithdateLabel, birthdateField, addressLabel, addressField, birthplaceLabel, birthplaceField, sexLabel, panel);

		signUpPanel.add(signUpButton);
		signUpPanel.add(loginButton);
		loginButton.setStyleName("buttonFloatRight");

		signUpButton.addClickHandler(event -> handleSignUpButton(emailField.getText(), passwordField.getText(), repeatPasswordField.getText(), 
				nameField.getText(), surnameField.getText(),usernameField.getText(),maleSexField.getValue()?"M":"F",birthdateField.getValue(), birthplaceField.getText(), addressField.getText())
				);


		loginButton.addClickHandler(event -> {
			Widget loginPanel = createLoginPanel();
			switchWidget(loginPanel);
		});

		return signUpPanel;
	}

	//Metodo handler per effettuare registrazione utente alla piattaforma Journal
	private void handleSignUpButton(String email, String password, String repeatedPassword, String name, String surname, String username, String sex, Date birthdate, String birthplace, String address) {
		if(email.trim().length() == 0) {
			Window.alert("Please insert a valid email");
			return;
		}

		if(password.trim().length()<5) {
			Window.alert("Password must be at least 5 characters long");
			return;
		}

		if(!password.trim().equals(repeatedPassword.trim())) {
			Window.alert("The two passwords don't match");
			return;
		}


		if(name.trim().length() == 0) {
			Window.alert("Please insert a valid name");
			return;
		}

		if(surname.trim().length() == 0) {
			Window.alert("Please insert a valid surname");
			return;
		}

		if(username.trim().length() == 0) {
			Window.alert("Please insert a valid username");
			return;
		}

		if(birthdate == null) {
			Window.alert("Please insert a valid bithdate");
			return;
		}

		if(address.trim().length() == 0) {
			Window.alert("Please insert a valid adress");
			return;
		}				

		userService.createUser(email, password, name, surname, username,sex, birthdate, birthplace, address, new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Sign up failed");
			}

			@Override
			public void onSuccess(User result) {
				if (Objects.nonNull(result)) {
					handleSignUpSuccessful(result);						
				} else {
					Window.alert("Sign up failed");
				}
			}
		});
	}

	//Metodo handle del sign up a con successo
	private void handleSignUpSuccessful(User user) {
		this.user = user;
		Window.alert(user.toString());
	}

	/* ------------------------------------------------------------------------------ */

	//Metodo di creazione pannello User
	private Widget createUserPanel() {
		final VerticalPanel userPanel = new VerticalPanel();

		final HorizontalPanel bodyPanel = new HorizontalPanel();		

		final HorizontalPanel emailTitlePanel = new HorizontalPanel();
		final Button returnHomeButton = new Button("Home");
		returnHomeButton.setStyleName("noMarginButton");

		final Label titleField = new Label("User data");

		final Label emailFieldLabel = new Label("Email:");
		final Label emailField = new Label(user.getEmail());
		emailField.setStyleName("labelProfile");

		final Label nameFieldLabel = new Label("Name:");
		final Label nameField = new Label(user.getName());
		nameField.setStyleName("labelProfile");

		final Label surnameFieldLabel = new Label("Surname:");
		final Label surnameField = new Label(user.getSurname());
		surnameField.setStyleName("labelProfile");

		final Label usernameFieldLabel = new Label("Username:");
		final Label usernameField = new Label(user.getUsername());
		usernameField.setStyleName("labelProfile");

		final Label sexFieldLabel = new Label("Sex:");
		final Label sexField = new Label(user.getSex());
		sexField.setStyleName("labelProfile");

		DateTimeFormat DateFor = DateTimeFormat.getFormat("dd/MM/yyyy");

		final Label dateFieldLabel = new Label("Date of birth:");
		final Label dateField = new Label(DateFor.format(user.getBirthdate()));
		dateField.setStyleName("labelProfile");

		final Label registratioDateFieldLabel = new Label("Registration Date:");
		final Label registrationDateField = new Label(DateFor.format(user.getRegistrationDate()));
		registrationDateField.setStyleName("labelProfile");

		final Label birthplaceFieldLabel = new Label("Birthplace:");
		final Label birthplaceField = new Label(user.getBirthplace());
		birthplaceField.setStyleName("labelProfile");

		final Label addressFieldLabel = new Label("Address:");
		final Label addressField = new Label(user.getAddress());
		addressField.setStyleName("labelProfile");

		final Label isPremiumFieldLabel = new Label("Premium:");
		final Label isPremiumField = new Label(user.isPremium()?"Subscribed":"Not Subscribed");
		isPremiumField.setStyleName("labelProfile");

		final Label creditCardFieldLabel = new Label("CreditCard:");
		final Label creditCardField = new Label(user.getCreditCard().toString());
		creditCardField.setStyleName("labelProfile");
		returnHomeButton.addClickHandler(event -> {
			viewHomePage(user);
		});


		final Button viewLikedArticleButton = new Button("View liked articles");
		viewLikedArticleButton.setStyleName("noMarginButton");
		viewLikedArticleButton.addClickHandler(event ->	{
			Widget viewLikedArticlePanel = createLikedArticlesPanel();
			bodyPanel.clear();
			addWidgetsToPanel(bodyPanel, viewLikedArticlePanel);
		});

		final Button viewCommentedArticleButton = new Button("View commented articles");
		viewCommentedArticleButton.setStyleName("noMarginButton");
		viewCommentedArticleButton.addClickHandler(event ->	{
			Widget viewCommentedArticlePanel = createCommentedArticlesPanel();
			bodyPanel.clear();
			addWidgetsToPanel(bodyPanel, viewCommentedArticlePanel);
		});

		final Button viewWrittenArticleButton = new Button("View written articles");
		viewWrittenArticleButton.setStyleName("noMarginButton");
		viewWrittenArticleButton.addClickHandler(event ->	{
			Widget viewWrittenArticle = createWrittenArticlesPanel();
			bodyPanel.clear();
			addWidgetsToPanel(bodyPanel, viewWrittenArticle);
		});

		addWidgetsToPanel(userPanel, returnHomeButton, titleField, emailTitlePanel, emailFieldLabel, emailField,
				nameFieldLabel, nameField,
				surnameFieldLabel, surnameField,
				usernameFieldLabel, usernameField,
				sexFieldLabel, sexField,
				dateFieldLabel, dateField,
				registratioDateFieldLabel, registrationDateField,
				birthplaceFieldLabel, birthplaceField,	
				addressFieldLabel, addressField,
				isPremiumFieldLabel, isPremiumField,
				creditCardFieldLabel, creditCardField,
				viewLikedArticleButton, viewCommentedArticleButton, viewWrittenArticleButton);

		if(user.isPremium() == false) {						
			final Button createPaymentButton = new Button("Abbonati");
			createPaymentButton.addClickHandler(event ->{
				Widget paymentPanel = createPaymentPanel();
				bodyPanel.clear();
				addWidgetsToPanel(bodyPanel, paymentPanel);
			});
			addWidgetsToPanel(userPanel, createPaymentButton);
		}

		addWidgetsToPanel(userPanel, bodyPanel);


		return userPanel;
	}

	//Metodo di visualizzazione home page
	private void viewHomePage(User user) {
		this.user = user;
		this.author = null;
		Widget homePagePanel = createArticlesPanel();
		switchWidget(homePagePanel);

	}

	//Metodo di visualizzazione profilo utente loggato
	private void viewUserProfile() {
		Widget userPanel = createUserPanel();
		switchWidget(userPanel);
	}

	/* ------------------------------------------------------------------------------ */

	//Creazione pannello di abbonamento
	private Widget createPaymentPanel() {
		final VerticalPanel paymentPanel = new VerticalPanel();
		paymentPanel.setStyleName("paymentPanel");
		final Button returnHomeButton = new Button("Home");
		final TextBox numberCreditCardField = new TextBox();
		final DateBox expirationDateCreditCardField = new DateBox();
		DateTimeFormat dateFormat = DateTimeFormat.getFormat("dd/MM/yyyy");
		expirationDateCreditCardField.setFormat(new DateBox.DefaultFormat(dateFormat));
		final IntegerBox cvvCreditCardField = new IntegerBox();
		cvvCreditCardField.setStyleName("integerBox");
		final TextBox nationCreditCardField = new TextBox();
		final IntegerBox capCreditCardField = new IntegerBox();
		capCreditCardField.setStyleName("integerBox");
		final Button addPaymentButton = new Button("Subscribe"); 

		numberCreditCardField.getElement().setAttribute("placeholder", "*number creditcard");
		expirationDateCreditCardField.getElement().setAttribute("placeholder", "*Expiration Date:");
		cvvCreditCardField.getElement().setAttribute("placeholder", "* cvv");
		nationCreditCardField.getElement().setAttribute("placeholder", "*nation");
		capCreditCardField.getElement().setAttribute("placeholder", "*cap");

		paymentPanel.add(returnHomeButton);
		paymentPanel.add(numberCreditCardField);
		paymentPanel.add(expirationDateCreditCardField);
		paymentPanel.add(cvvCreditCardField);
		paymentPanel.add(nationCreditCardField);
		paymentPanel.add(capCreditCardField);
		paymentPanel.add(addPaymentButton);
		returnHomeButton.addClickHandler(event -> {
			viewHomePage(user);
		});

		addPaymentButton.addClickHandler(event -> {


			if(numberCreditCardField.getText().trim().length()!=16) {
				Window.alert("Please insert a valid Credit card number");
				return;
			}

			if(Objects.isNull(expirationDateCreditCardField.getValue()) || expirationDateCreditCardField.getValue().before(new Date())) {
				Window.alert("Please insert a valid expiration date");
				return;
			}

			if( Objects.isNull(cvvCreditCardField.getValue())|| cvvCreditCardField.getValue().intValue()<0 || cvvCreditCardField.getValue().intValue()>=999) {
				Window.alert("Please insert a valid cvv");
				return;
			}

			if(nationCreditCardField.getText().trim().isEmpty()) {
				Window.alert("Please insert a valid nation");
				return;
			}

			if(Objects.isNull(capCreditCardField.getValue()) || capCreditCardField.getValue().intValue()>=100000) {
				Window.alert("Please insert a valid zip code");
				return;
			}			


			handleAddPaymentButton(numberCreditCardField.getText(), expirationDateCreditCardField.getValue(), cvvCreditCardField.getValue().intValue(), nationCreditCardField.getText(), capCreditCardField.getValue().intValue());
		});


		return paymentPanel;
	}

	//Metodo handler per creazione oggetto CreditCard da abbinare ad utente abbonato
	private void handleAddPaymentButton(String number, Date expirationDate, int cvv, String nation, int cap) {

		CreditCard creditcard = new CreditCard(number, user.getName(), user.getSurname(), expirationDate, cvv, nation, cap);
		userService.createCreditCard(creditcard, user.getId(), new AsyncCallback<CreditCard>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Credit card not added");

			}

			@Override
			public void onSuccess(CreditCard result) {
				if (Objects.nonNull(result)) {
					Window.alert("Credit card added: "+result.getName() );
					user.setCreditCard(result);
					user.setPremium(true);
					viewUserProfile();
				} else {
					Window.alert("Credit card not added");
				}

			}
		});
	}

	/* ------------------------------------------------------------------------------ */

	//Metodo di creazione taella articoli
	private Widget createArticlesPanel() {
		final HorizontalPanel horizontalPanel = new HorizontalPanel();
		final VerticalPanel verticalPanel = new VerticalPanel();

		this.articlesTable = new FlexTable();
		refreshArticles();

		if(!Objects.isNull(user)) {
			final Button viewProfileButton = new Button("View your profile");
			viewProfileButton.setStyleName("noMarginButton");
			viewProfileButton.addClickHandler(event -> {
				viewUserProfile();
			});
			addWidgetsToPanel(verticalPanel, viewProfileButton);
		}

		this.categoriesPanel = new VerticalPanel();
		if(!Objects.isNull(user) && this.user.isAdmin()) {
			Widget nestedCategoriesPanel = createCategoriesPanel();
			this.categoriesPanel.add(nestedCategoriesPanel);			
		}
		Widget createArticlePanel = createArticlePanel(null);
		this.saveArticlePanel = new VerticalPanel();
		this.saveArticlePanel.add(createArticlePanel);
		addWidgetsToPanel(verticalPanel, categoriesPanel, articlesTable, saveArticlePanel);

		this.articlePanel = new VerticalPanel();
		addWidgetsToPanel(horizontalPanel, verticalPanel, articlePanel);

		return horizontalPanel;
	}

	//Metodo di creazione articolo
	private Widget createArticlePanel(Article article) {
		final VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("createArticlePanel");

		final HorizontalPanel titlePanel = new HorizontalPanel();
		final Label titleFieldLabel = new Label("Title:");
		final TextBox titleField = new TextBox();
		titleField.setText(Objects.nonNull(article)?article.getTitle():"");
		addWidgetsToPanel(titlePanel, titleFieldLabel, titleField);

		final HorizontalPanel bodyPanel = new HorizontalPanel();
		final Label bodyFieldLabel = new Label("Text: ");
		final TextArea bodyField = new TextArea();
		bodyField.setText(Objects.nonNull(article)?article.getBody():"");
		addWidgetsToPanel(bodyPanel, bodyFieldLabel, bodyField);

		final CheckBox premiumField = new CheckBox("Premium");
		premiumField.setEnabled(false);
		premiumField.setValue(Objects.nonNull(article)?article.isPremium():false);
		if(Objects.nonNull(user)&&(user.isPremium()||user.isAdmin())) {
			premiumField.setEnabled(true);
		}
		final ListBox categoryListBox = new ListBox();


		categoryService.getCategories(new AsyncCallback<Set<Category>>() {

			@Override
			public void onSuccess(Set<Category> result) {
				List<Category> categories = new ArrayList<Category>(result);
				for (int i=0; i<categories.size();i++) {

					if (!categories.get(i).getName().equals("root")) {
						categoryListBox.addItem(categories.get(i).getName());
					}

					if(Objects.nonNull(article)&&Objects.equals(article.getCategoryName(), categories.get(i).getName())) {
						categoryListBox.setSelectedIndex(i);
					}
				}

			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to load get categories!");

			}
		});
		final Button createArticleButton = new Button("Create article");
		final Button updateArticleButton = new Button("update");
		final Button cancelArticleButton = new Button("cancel");

		if(Objects.isNull(article)) {
			addWidgetsToPanel(verticalPanel, titlePanel, bodyPanel, categoryListBox, premiumField, createArticleButton);
		}else {
			addWidgetsToPanel(verticalPanel, titlePanel, bodyPanel, categoryListBox, premiumField, updateArticleButton, cancelArticleButton);
		}

		createArticleButton.addClickHandler(event -> handleCreateArticle(titleField, bodyField, categoryListBox, premiumField));
		updateArticleButton.addClickHandler(event -> handelUpdateArticlePanel(article, titleField, bodyField, categoryListBox, premiumField));
		cancelArticleButton.addClickHandler(event -> handleCancelArticlePanel());

		Widget authPanel = createAuthPanel();
		addWidgetsToPanel(verticalPanel, authPanel);


		return verticalPanel;
	}

	//Creazione pannello autore per consentire all' utente loggato di poter creare un articolo
	private Widget createAuthPanel() {

		final VerticalPanel authPanel = new VerticalPanel();

		final Button loginButton = new Button("Login");
		loginButton.setStyleName("buttonFloatRight");
		final Button signUpButton = new Button("Create an account");
		signUpButton.setStyleName("buttonFloatRight");
		final Button logoutButton = new Button("Logout");

		if(Objects.isNull(user)) {
			authPanel.add(loginButton);
			authPanel.add(signUpButton);			
		} else {
			authPanel.add(logoutButton);
		}


		loginButton.addClickHandler(event -> {
			Widget loginPanel = createLoginPanel();
			switchWidget(loginPanel);
		});

		signUpButton.addClickHandler(event -> {
			Widget signUpPanel = createSignUpPanel();
			switchWidget(signUpPanel);
		});


		logoutButton.addClickHandler(event -> {
			user=null;
			Widget articlesPanel = createArticlesPanel();
			switchWidget(articlesPanel);

			Widget createArticlePanel = createArticlePanel(null);
			this.saveArticlePanel.clear();
			this.saveArticlePanel.add(createArticlePanel);
		});

		return authPanel;
	}
	
	//Metodo handler per la cancellazione dell' articolo selezionato
	private void handleCancelArticlePanel() {
		Widget createArticlePanel = createArticlePanel(null);
		this.saveArticlePanel.clear();
		this.saveArticlePanel.add(createArticlePanel);
	}

	//Metodo handler per la modifica dell' articolo selezionato
	private void handelUpdateArticlePanel(Article article, TextBox titleField, TextArea bodyField, ListBox categoryListBox, CheckBox premiumField) {
		String title = titleField.getText();
		String body = bodyField.getText();
		String category = categoryListBox.getSelectedValue();
		boolean premium = premiumField.getValue();
		articleService.updateArticle(article.getId(), body, premium, title, category, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to create article");
			}

			@Override
			public void onSuccess(Article result) {
				refreshArticles();
				handleCancelArticlePanel();
			}
		});

	}

	//Metodo handler per la creazione di un articolo
	private void handleCreateArticle(TextBox titleField, TextArea bodyField, ListBox categoryListBox, CheckBox premiumField) {
		if(Objects.nonNull(this.user)) {
			String title = titleField.getText();
			String body = bodyField.getText();
			String category = categoryListBox.getSelectedValue();
			boolean premium = premiumField.getValue();

			if (title.isEmpty()) {
				Window.alert("Invalid title");
				titleField.setFocus(true);
				return;
			}

			if (body.isEmpty()) {
				Window.alert("Invalid body");
				bodyField.setFocus(true);
				return;
			}

			if (Objects.isNull(category)) {
				Window.alert("Invalid category");
				categoryListBox.setFocus(true);
				return;
			}

			articleService.createArticle(title, body, user.getUsername(), category, premium, new AsyncCallback<Article>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Failed to create article");
				}

				@Override
				public void onSuccess(Article result) {
					titleField.setText("");
					bodyField.setText("");
					premiumField.setValue(false);
					refreshArticles();
				}
			});

		}else {
			Widget loginPanel = createLoginPanel();	
			switchWidget(loginPanel);}
	}

	//Metodo di settaggio dell' intestazione tabella articoli
	private void addHeaderToArticleTable() {
		articlesTable.setText(0, 0, "Title");
		articlesTable.setText(0, 1, "Author");
		articlesTable.setText(0, 2, "Publication date");
		articlesTable.setText(0, 3, "Category");
		articlesTable.setText(0, 4, "Premium");

		articlesTable.getCellFormatter().addStyleName(0, 0, "watchListHeader");
		articlesTable.getCellFormatter().addStyleName(0, 1, "watchListHeader");
		articlesTable.getCellFormatter().addStyleName(0, 2, "watchListHeader");
		articlesTable.getCellFormatter().addStyleName(0, 3, "watchListHeader");
		articlesTable.getCellFormatter().addStyleName(0, 4, "watchListHeader");
	}

	//Metodo di aggiunta articolo a tabella articoli
	private void addArticleToTable(Article article) {
		int row = articlesTable.getRowCount();

		if(Objects.nonNull(user) && !user.getUsername().equals(article.getAuthor()) && Objects.isNull(author)) {
			HorizontalPanel horizontalPanel = new HorizontalPanel();
			Button viewAuthorProfileButton = new Button("View Profile");
			viewAuthorProfileButton.setStyleName("noMarginButton");
			final Label authorLabel = new Label(article.getAuthor());
			viewAuthorProfileButton.addClickHandler(event-> viewAuthorProfile(article.getAuthor()));
			addWidgetsToPanel(horizontalPanel, viewAuthorProfileButton, authorLabel);
			articlesTable.setWidget(row, 1, horizontalPanel);
		} else {
			articlesTable.setText(row, 1, article.getAuthor());
		}

		articlesTable.setText(row, 0, article.getTitle());

		articlesTable.setText(row, 2, article.getPublicationDate().toString());
		articlesTable.setText(row, 3, article.getCategoryName());
		articlesTable.setText(row, 4, article.isPremium() ? "Yes" : "No");

		articlesTable.getCellFormatter().addStyleName(row, 0, "watchListNumericColumn");
		articlesTable.getCellFormatter().addStyleName(row, 1, "watchListNumericColumn");
		articlesTable.getCellFormatter().addStyleName(row, 2, "watchListNumericColumn");
		articlesTable.getCellFormatter().addStyleName(row, 3, "watchListNumericColumn");
		articlesTable.getCellFormatter().addStyleName(row, 4, "watchListNumericColumn");

		Button openArticleButton = new Button("Open");
		openArticleButton.setStyleName("openButton");
		final Button deleteArticleButton = new Button("delete");
		final Button updateArticleButton = new Button("update");
		openArticleButton.addStyleDependentName("open");

		openArticleButton.addClickHandler(event ->{ 
			if(!article.isPremium()||(Objects.nonNull(this.user)&&(this.user.isPremium()||this.user.isAdmin()))) {
				createViewArticlePanel(article);
			}else if(article.isPremium()&& Objects.nonNull(this.user)&&(!this.user.isPremium()||!this.user.isAdmin())){
				Window.alert("Must be subscribed! You'll be redirected to the subscription page");
				switchWidget(createPaymentPanel());
			}else {
				Window.alert("Must be logged! You'll be redirected to the login/sign up page");
				switchWidget(createLoginPanel());
			}
		});

		updateArticleButton.addClickHandler(event -> createUpdateArticlePanel(article));
		deleteArticleButton.addClickHandler(event ->deleteArticle(article.getId()));

		articlesTable.setWidget(row, 5, openArticleButton);
		if(Objects.nonNull(user)&&user.isAdmin()) {
			articlesTable.setWidget(row, 6, deleteArticleButton);	
			articlesTable.setWidget(row, 7, updateArticleButton);			

		}

	}
	
	//Metodo di creazione pannello di visualizzazione articolo
	private void createViewArticlePanel(Article article) {

		this.articlePanel.clear();
		final VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("commentPanel");

		final HorizontalPanel titlePanel = new HorizontalPanel();
		final Label titleFieldLabel = new Label("Title:");
		final Label titleContentLabel = new Label(article.getTitle());
		titleContentLabel.setStyleName("labelProfile");
		addWidgetsToPanel(titlePanel, titleFieldLabel, titleContentLabel);

		final HorizontalPanel bodyPanel = new HorizontalPanel();
		final Label bodyFieldLabel = new Label("Text: ");
		final Label bodyContentLabel = new Label(article.getBody());
		bodyContentLabel.setStyleName("labelTextArticle");
		addWidgetsToPanel(bodyPanel, bodyFieldLabel, bodyContentLabel);

		Button likeButton;
		Label likeCountLabel;

		int size = article.getLikes().size();

		if (size > 0) {
			Like like = article.getLikes().stream()
					.filter((currentLike) -> currentLike.getIdUser()==user.getId())
					.findFirst()
					.orElse(null);

			if(Objects.isNull(like)) {
				likeButton = new Button("Like");
				likeButton.setStyleName("buttonFloatRight");
				likeCountLabel = new Label("Total likes: "+article.getLikes().size());
				likeCountLabel.setStyleName("labelLike");
				likeButton.addClickHandler(event -> {if(Objects.nonNull(this.user)) {
					createLike(article);
				}else{
					switchWidget(createLoginPanel());
				}});			
			} else {
				likeButton = new Button("Dislike");
				likeButton.setStyleName("buttonFloatRight");
				likeCountLabel = new Label("Total likes: "+article.getLikes().size());
				likeCountLabel.setStyleName("labelLike");
				likeButton.addClickHandler(event -> removeLike(article));	
			}

		} else {
			likeButton = new Button("Like");
			likeButton.setStyleName("buttonFloatRight");
			likeCountLabel = new Label("Total likes: "+article.getLikes().size());
			likeCountLabel.setStyleName("labelLike");
			likeButton.addClickHandler(event -> {if(Objects.nonNull(this.user)) {
				createLike(article);
			}else{
				switchWidget(createLoginPanel());
			}});			
		}		
		addWidgetsToPanel(verticalPanel, titlePanel, bodyPanel, likeButton, likeCountLabel);
		Stream<Comment> comments = article.getComments().stream();

		boolean isAdminUser = (Objects.nonNull(this.user)&&this.user.isAdmin());
		if(!isAdminUser) {
			comments = comments.filter(comment-> comment.isApproved());
		} 
		comments.forEach(comment -> {
			final HorizontalPanel commentPanel = new HorizontalPanel();
			final Label authorFieldLabel = new Label(comment.getEmail());
			final Label textContentLabel = new Label(comment.getText());
			textContentLabel.setStyleName("labelTextComment");

			addWidgetsToPanel(commentPanel, authorFieldLabel, textContentLabel);

			if(isAdminUser) {

				if(!comment.isApproved()) {
					final Button approveCommentButton = new Button("Accept");
					approveCommentButton.addClickHandler(event->approveComment(article.getId(), comment.getId()));
					addWidgetsToPanel(commentPanel, approveCommentButton);
				}


				final Button deleteCommentButton = new Button("Delete");
				deleteCommentButton.addClickHandler(event->deleteComment(article.getId(), comment.getId()));

				addWidgetsToPanel(commentPanel, deleteCommentButton);
			}

			verticalPanel.add(commentPanel);
		});

		final HorizontalPanel createCommentPanel = new HorizontalPanel();
		final Label createCommentLabel = new Label("Text: ");
		final TextArea createCommentField = new TextArea();
		final Button createCommentButton = new Button("Create comment");	
		createCommentButton.addClickHandler(event -> {if(Objects.nonNull(this.user)) {
			createComment(article, createCommentField);
		}else{
			switchWidget(createLoginPanel());
		}
		});

		addWidgetsToPanel(createCommentPanel, createCommentLabel, createCommentField,createCommentButton);
		verticalPanel.add(createCommentPanel);
		this.articlePanel.add(verticalPanel);



	}

	//Metodo update per articolo modificato da utente amministratore
	private void createUpdateArticlePanel(Article article) {
		Widget createArticlePanel = createArticlePanel(article);
		this.saveArticlePanel.clear();
		this.saveArticlePanel.add(createArticlePanel);
	}

	//Metodo di visualizzazione profilo autore
	private void viewAuthorProfile(String username) {		
		userService.getUserByUsername(username, new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to get Author Profile");

			}

			@Override
			public void onSuccess(User result) {
				author = result;
				Widget authorPanel = createAuthorPanel(result);
				switchWidget(authorPanel);
			}
		});

	}

	//Metodo per la visualizzazione scheda autore 
	private Widget createAuthorPanel(User author) {

		final VerticalPanel authorPanel = new VerticalPanel();
		final HorizontalPanel bodyPanel = new HorizontalPanel();	

		final Button returnHomeButton = new Button("Home");

		final Label titleField = new Label("Author data");

		final Label nameFieldLabel = new Label("Name:");
		final Label nameField = new Label(author.getName());

		final Label surnameFieldLabel = new Label("Surname:");
		final Label surnameField = new Label(author.getSurname());

		DateTimeFormat DateFor = DateTimeFormat.getFormat("dd/MM/yyyy");

		final Label registratioDateFieldLabel = new Label("Registration Date:");
		final Label registrationDateField = new Label(DateFor.format(author.getRegistrationDate()));

		returnHomeButton.addClickHandler(event -> {
			viewHomePage(user);
		});

		final Button viewWrittenArticleButton = new Button("View written articles");
		viewWrittenArticleButton.addClickHandler(event ->	{

			articlesTable.removeAllRows();
			final HorizontalPanel viewWrittenArticle = new HorizontalPanel();
			final VerticalPanel verticalPanel = new VerticalPanel();
			this.articlesTable = new FlexTable();

			userService.getWrittenArticles(author.getId(), new AsyncCallback<Set<Article>>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Failed to load written articles!");
				}
				@Override
				public void onSuccess(Set<Article> articles) {
					addHeaderToArticleTable();
					if(user.isPremium()) {
						articles.forEach(article -> addArticleToTable(article));
					} else {
						articles.stream().filter(article -> Objects.equals(article.isPremium(), false)).forEach(article -> addArticleToTable(article));
					}
				}
			});

			Widget createRecentArticlePanel = createRecentArticlePanel();
			addWidgetsToPanel(verticalPanel, articlesTable, createRecentArticlePanel);
			this.articlePanel = new VerticalPanel();
			addWidgetsToPanel(viewWrittenArticle, verticalPanel, articlePanel);

			bodyPanel.clear();
			addWidgetsToPanel(bodyPanel, viewWrittenArticle);
		});

		addWidgetsToPanel(authorPanel, returnHomeButton, titleField,
				nameFieldLabel, nameField,
				surnameFieldLabel, surnameField,
				registratioDateFieldLabel, registrationDateField,
				viewWrittenArticleButton);

		addWidgetsToPanel(authorPanel, bodyPanel);


		return authorPanel;
	}

	//Metodo di raccolta articoli a cui l' utente ha messo like, commentato e/o scritto
	private void refreshRecentActivityArticles(String recentActivity) {
		System.out.println("Sto refreshando");
		articlesTable.removeAllRows();
		if(recentActivity.equals("likedArticles")) {
			userService.getLikedArticles(user.getId(), new AsyncCallback<Set<Article>>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Failed to load liked articles!");
				}
				@Override
				public void onSuccess(Set<Article> result) {
					addHeaderToArticleTable();
					result.forEach(article -> addArticleToTable(article));
				}
			});
		}else if(recentActivity.equals("commentedArticles")) {
			userService.getCommentedArticles(user.getId(), new AsyncCallback<Set<Article>>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Failed to load commented articles!");
				}
				@Override
				public void onSuccess(Set<Article> result) {
					addHeaderToArticleTable();
					result.forEach(article -> addArticleToTable(article));
				}
			});
		}else if(recentActivity.equals("writtenArticles")) {
			userService.getWrittenArticles(user.getId(), new AsyncCallback<Set<Article>>() {
				@Override
				public void onFailure(Throwable caught) {
					Window.alert("Failed to load written articles!");
				}
				@Override
				public void onSuccess(Set<Article> result) {
					addHeaderToArticleTable();
					result.forEach(article -> addArticleToTable(article));
				}
			});
		}
	}

	//Metodo refresh per la tabella articoli
	private void refreshArticles() {
		articlesTable.removeAllRows();

		articleService.getArticles(new AsyncCallback<Set<Article>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to load articles!");
			}
			@Override
			public void onSuccess(Set<Article> result) {
				addHeaderToArticleTable();
				List<Article> articles = new ArrayList<Article>(result);
				articles.sort((a1,a2) -> a2.getPublicationDate().compareTo(a1.getPublicationDate()));
				articles.forEach(article ->addArticleToTable(article));
			}
		});
	}

	//Metodo per la cancellazione articolo selezionato
	private void deleteArticle(String idArticle) {
		articleService.deleteArticle(idArticle, new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to delete Article");

			}

			@Override
			public void onSuccess(Boolean result) {
				Window.alert("Delete article succesfully");
				refreshArticles();

			}
		});
	}

	//Metodo per consentire all' utente registrato di esprimere like ad un articolo
	private void createLike(Article article) {
		likeService.createLike(article.getId(), user.getId(), new AsyncCallback<Article>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to create like");

			}

			@Override
			public void onSuccess(Article result) {
				createViewArticlePanel(result);

			}
		});

	}

	//Metodo che consente a utente di rimuovere like da un articolo a cui aveva espresso gradimento
	private void removeLike(Article article) {

		likeService.removeLike(article.getId(), user.getId(), new AsyncCallback<Article>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to remove like");

			}

			@Override
			public void onSuccess(Article result) {
				System.out.println("entrato nell onsuccess");
				createViewArticlePanel(result);

			}
		});
	}

	//Metodo di creazione del pannello raggruppante gli articoli scritti da autore selezionato/utente loggato
	private Widget createWrittenArticlesPanel() {
		articlesTable.removeAllRows();
		final HorizontalPanel horizontalPanel = new HorizontalPanel();
		final VerticalPanel verticalPanel = new VerticalPanel();
		this.articlesTable = new FlexTable();
		refreshRecentActivityArticles("writtenArticles");
		Widget createRecentArticlePanel = createRecentArticlePanel();
		addWidgetsToPanel(verticalPanel, articlesTable, createRecentArticlePanel);
		this.articlePanel = new VerticalPanel();
		addWidgetsToPanel(horizontalPanel, verticalPanel, articlePanel);
		return horizontalPanel;
	}

	//Metodo di creazione del pannello raggruppante gli articoli a cui l' utente loggato ha messo like
	private Widget createLikedArticlesPanel() {
		articlesTable.removeAllRows();
		final HorizontalPanel horizontalPanel = new HorizontalPanel();
		final VerticalPanel verticalPanel = new VerticalPanel();
		this.articlesTable = new FlexTable();
		refreshRecentActivityArticles("likedArticles");
		Widget createRecentArticlePanel = createRecentArticlePanel();
		addWidgetsToPanel(verticalPanel, articlesTable, createRecentArticlePanel);
		this.articlePanel = new VerticalPanel();
		addWidgetsToPanel(horizontalPanel, verticalPanel, articlePanel);
		return horizontalPanel;
	}

	//Metodo di creazione del pannello raggruppante gli articoli recenti
	private Widget createRecentArticlePanel() {
		final VerticalPanel verticalPanel = new VerticalPanel();
		final HorizontalPanel titlePanel = new HorizontalPanel();
		final HorizontalPanel bodyPanel = new HorizontalPanel();
		addWidgetsToPanel(verticalPanel, titlePanel, bodyPanel);		
		return verticalPanel;
	}

	//Metodo di creazione del pannello raggruppante gli articoli commentati da utente loggato
	private Widget createCommentedArticlesPanel() {
		articlesTable.removeAllRows();
		final HorizontalPanel horizontalPanel = new HorizontalPanel();
		final VerticalPanel verticalPanel = new VerticalPanel();
		this.articlesTable = new FlexTable();
		refreshRecentActivityArticles("commentedArticles");
		Widget createRecentArticlePanel = createRecentArticlePanel();
		addWidgetsToPanel(verticalPanel, articlesTable, createRecentArticlePanel);
		this.articlePanel = new VerticalPanel();
		addWidgetsToPanel(horizontalPanel, verticalPanel, articlePanel);
		return horizontalPanel;
	}

	/* ------------------------------------------------------------------------------ */

	//Metodo di creazione taella categorie
	private Widget createCategoriesPanel() {
		final HorizontalPanel horizontalPanel = new HorizontalPanel();

		final VerticalPanel verticalPanel = new VerticalPanel();

		this.categoriesTable = new FlexTable();
		refreshCategories();

		Widget createCategoryPanel = createCategoryPanel();
		addWidgetsToPanel(verticalPanel, categoriesTable, createCategoryPanel);

		this.categoryPanel = new VerticalPanel();

		addWidgetsToPanel(horizontalPanel, verticalPanel, categoryPanel);

		return horizontalPanel;
	}

	//Metodo handler che consente l' eliminazione di una categoria
	private void handleDeleteCategoryButton(String name) {
		categoryService.deleteCategory(name, new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Category not deleted");				
			}

			@Override
			public void onSuccess(Boolean result) {
				if(Objects.isNull(result)) {
					Window.alert("Category not found");
				} else {					
					Window.alert("Category deleted");
					refreshCategories();
				}


			}
		});
	}

	//Metodo handler che consente la modifica di una categoria
	private void handleUpdateCategoryButton(String oldName, String newName) {
		categoryService.updateCategory(oldName, newName, new AsyncCallback<Category>() {

			@Override
			public void onSuccess(Category result) {
				if(Objects.isNull(result)) {
					Window.alert("Category not found");
				} else {					
					Window.alert("Category name updated");
					refreshCategories();
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Category name not  updated");				
			}
		});
	}

	//Metodo per la creazione di una categoria/sottocategoria
	private Widget createCategoryPanel() {
		final VerticalPanel verticalPanel = new VerticalPanel();
		verticalPanel.setStyleName("loginPanel");
		final HorizontalPanel titlePanel = new HorizontalPanel();
		final Label nameCategoryFieldLabel = new Label("Category name:");
		final TextBox nameCategoryField = new TextBox();
		addWidgetsToPanel(titlePanel, nameCategoryFieldLabel, nameCategoryField);
		final Label parentCategoryTitleSelected = new Label("Select parent of category:");
		final ListBox parentCategorySelected = new ListBox();

		categoryService.getCategories(new AsyncCallback<Set<Category>>() {

			@Override
			public void onSuccess(Set<Category> result) {
				result.forEach(category -> parentCategorySelected.addItem(category.getName()));				
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to load get categories!");

			}
		});

		final Button createCategoryButton = new Button("Create category");

		createCategoryButton.addClickHandler(event -> handleCreateCategory(nameCategoryField, parentCategorySelected));

		addWidgetsToPanel(verticalPanel, titlePanel,parentCategoryTitleSelected, parentCategorySelected, createCategoryButton);

		return verticalPanel;
	}

	//Metodo handler per la creazione di una categoria/sottocategoria
	private void handleCreateCategory(TextBox nameCategoryField, ListBox parentCategorySelected) {
		String nameCategory = nameCategoryField.getText();
		String parentCategory = parentCategorySelected.getSelectedValue();

		categoryService.createCategory(nameCategory, parentCategory, new AsyncCallback<Category>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to load category");		
			}

			@Override
			public void onSuccess(Category result) {
				Window.alert("Category created successfully!!");
				Widget articlesPanel = createArticlesPanel();
				switchWidget(articlesPanel);
			}
		});
	}

	//Metodo di settaggio dell' intestazione tabella categoria
	private void addHeaderToCategoryTable() {
		categoriesTable.setText(0, 0, "Name");
		categoriesTable.setText(0, 1, "Parent");
		categoriesTable.setText(0, 2, "New name");

		categoriesTable.getCellFormatter().addStyleName(0, 0, "watchListHeader");
		categoriesTable.getCellFormatter().addStyleName(0, 1, "watchListHeader");
		categoriesTable.getCellFormatter().addStyleName(0, 2, "watchListHeader");
	}

	//Metodo di aggiunta della categoria/sottocategoria alla tabella categoria
	private void addCategoryToTable(Category category) {
		int row = categoriesTable.getRowCount();

		final Button updateCategoryButton = new Button("update");
		final Button deleteCategoryButton = new Button("delete");
		final TextBox updateNameCategoryTextBox = new TextBox();
		updateNameCategoryTextBox.getElement().setAttribute("placeholder", "Insert new name");
		categoriesTable.setText(row, 0, category.getName());
		categoriesTable.setText(row, 1, Objects.isNull(category.getParent())? "----":category.getParent().getName());
		categoriesTable.setWidget(row, 2, updateNameCategoryTextBox);

		updateCategoryButton.addClickHandler(event -> handleUpdateCategoryButton(category.getName(), updateNameCategoryTextBox.getText()));
		deleteCategoryButton.addClickHandler(event -> handleDeleteCategoryButton(category.getName()));

		if(!category.getName().equals("root")) {
			categoriesTable.setWidget(row, 3, updateCategoryButton);
			categoriesTable.setWidget(row, 4, deleteCategoryButton);
		}

		categoriesTable.getCellFormatter().addStyleName(row, 0, "watchListNumericColumn");
		categoriesTable.getCellFormatter().addStyleName(row, 1, "watchListNumericColumn");
		categoriesTable.getCellFormatter().addStyleName(row, 2, "watchListNumericColumn");
		categoriesTable.getCellFormatter().addStyleName(row, 3, "watchListNumericColumn");
		categoriesTable.getCellFormatter().addStyleName(row, 4, "watchListNumericColumn");

	}

	//Metodo refresh per la tabella categorie
	private void refreshCategories() {
		categoriesTable.removeAllRows();

		categoryService.getCategories(new AsyncCallback<Set<Category>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to load articles!");
			}
			@Override
			public void onSuccess(Set<Category> result) {
				addHeaderToCategoryTable();
				result.forEach(category -> addCategoryToTable(category));
			}
		});
	}

	/* ------------------------------------------------------------------------------ */

	//Metodo per l' approvazione commento da utente amministratore
	private void approveComment(String idArticle, String idComment) {
		commentService.approveComment(idArticle, idComment, new AsyncCallback<Article>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to approve comment");

			}

			@Override
			public void onSuccess(Article result) {
				createViewArticlePanel(result);
			}
		});
	}

	//Metodo per la cancellazione di un commento da utente amministratore
	private void deleteComment(String idArticle, String idComment) {
		commentService.deleteComment(idArticle, idComment, new AsyncCallback<Article>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to delete comment");

			}

			@Override
			public void onSuccess(Article result) {
				createViewArticlePanel(result);

			}
		});
	}

	//Metodo che consente di creare un commento a specifico articolo
	private void createComment(Article article, TextArea createCommentField) {
		commentService.createComment(article.getId(), user.getEmail(), createCommentField.getText(), new AsyncCallback<Article>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Failed to create comment");
			}

			@Override
			public void onSuccess(Article result) {
				createViewArticlePanel(result);
			}
		});
	}


}
