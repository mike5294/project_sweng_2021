package com.journal.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.journal.shared.Article;

@RemoteServiceRelativePath("likes")
public interface LikeService extends RemoteService{
	
	Article createLike(String idArticle, String idUser);
	
	Article removeLike(String idArticle, String idUser);
}
