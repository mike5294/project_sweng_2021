package com.journal.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.journal.shared.Article;


public interface LikeServiceAsync {
	void createLike (String idArticle, String idUser , AsyncCallback<Article> callback);
	void removeLike (String idArticle, String idUser , AsyncCallback<Article> callback);
}
