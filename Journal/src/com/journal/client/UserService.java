package com.journal.client;

import java.util.Date;
import java.util.Set;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.journal.shared.Article;
import com.journal.shared.CreditCard;
import com.journal.shared.User;

@RemoteServiceRelativePath("users")
public interface UserService extends RemoteService {
	
	User getUser(String email, String password);
	
	User getUserByUsername(String username);

	User createUser(String email, String password, String name, String surname, String username, String sex, Date birthdate, String birthplace, String address);

	Set<Article> getLikedArticles(String id);
	
	Set<Article> getCommentedArticles(String id);
	
	Set<Article> getWrittenArticles(String id);
	
	CreditCard createCreditCard(CreditCard creditcard, String id);
}