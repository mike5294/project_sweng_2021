package com.journal.client;

import java.util.Date;
import java.util.Set;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.journal.shared.Article;
import com.journal.shared.CreditCard;
import com.journal.shared.User;

public interface UserServiceAsync{
	
	void getUser(String email, String password, AsyncCallback<User> callback);
	
	void getUserByUsername(String username, AsyncCallback<User> callback);
	
	void createUser(String email, String password, String name, String surname, String username, String sex, Date birthdate, String birthplace, String address, AsyncCallback<User> callback);
	
	void getLikedArticles(String id, AsyncCallback<Set<Article>> callback);
	
	void getCommentedArticles(String id, AsyncCallback<Set<Article>> callback);
	
	void getWrittenArticles(String id, AsyncCallback<Set<Article>> callback);

	void createCreditCard(CreditCard creditcard, String id, AsyncCallback<CreditCard> callback);
}