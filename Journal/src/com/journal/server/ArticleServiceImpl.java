package com.journal.server;


import java.util.Set;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.journal.client.ArticleService;
import com.journal.shared.Article;

public class ArticleServiceImpl extends RemoteServiceServlet implements ArticleService{

	private static final long serialVersionUID = 1L;

	@Override
	public Set<Article> getArticles() {
		return DatabaseReferences.getArticles();
	}
	
	@Override
	public Article getArticleById(String id) {
		return DatabaseReferences.getArticleById(id);
	}

	@Override
	public Article createArticle(String title, String body, String author, String categoryName, boolean premium) {
		Article article = new Article(title, body, author, categoryName, premium);
		boolean result = DatabaseReferences.createArticle(article);
		return result?article:null;
	}
	@Override
	public Article updateArticle(String idArticle, String body, boolean premium, String title, String category) {
		Article article = DatabaseReferences.getArticleById(idArticle);
		article.setBody(body);
		article.setPremium(premium);
		article.setTitle(title);
		article.setCategoryName(category);
		
		boolean result = DatabaseReferences.updateArticle(article);
		return result?article:null;
	}

	@Override
	public Boolean deleteArticle(String idArticle) {
		return DatabaseReferences.deleteArticle(idArticle);	
	}}
