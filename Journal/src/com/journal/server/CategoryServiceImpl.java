package com.journal.server;

import java.util.Set;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.journal.client.CategoryService;
import com.journal.shared.Category;

public class CategoryServiceImpl extends RemoteServiceServlet implements CategoryService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Set<Category> getCategories() {
		return DatabaseReferences.getCategories();
	}
	
	public Category getCategoryByName(String name) {
		return DatabaseReferences.getCategoryByName(name);
	}
	
	@Override
	public Category createCategory(String name, String parent) {
		DatabaseReferences.createCategory(name, parent);
		return DatabaseReferences.getCategoryRoot();
	}

	@Override
	public Category updateCategory(String oldName, String newName) {
		return DatabaseReferences.updateCategory(oldName, newName);
	}
	

	@Override
	public Boolean deleteCategory(String name) {
		return DatabaseReferences.deleteCategory(name);
	}

	

}
