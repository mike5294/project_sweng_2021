package com.journal.server;


import java.util.Date;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.journal.client.CommentService;
import com.journal.shared.Article;
import com.journal.shared.Comment;

public class CommentServiceImpl extends RemoteServiceServlet implements CommentService{
	
	private static final long serialVersionUID = 1L;


	@Override
	public Article createComment(String articleID, String email, String text) {
		Comment comment = new Comment(email, text, false, new Date());
		DatabaseReferences.createComment(articleID, comment);
		return DatabaseReferences.getArticleById(articleID);
	}


	@Override
	public Article approveComment(String idArticle, String idComment) {
		DatabaseReferences.approveComment(idArticle, idComment);
		return DatabaseReferences.getArticleById(idArticle);
	}


	@Override
	public Article deleteComment(String idArticle, String idComment) {
		DatabaseReferences.deleteComment(idArticle, idComment);
		return DatabaseReferences.getArticleById(idArticle);
	}
	
	

}
