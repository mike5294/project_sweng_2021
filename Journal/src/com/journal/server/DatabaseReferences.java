package com.journal.server;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.journal.shared.Article;
import com.journal.shared.Category;
import com.journal.shared.Comment;
import com.journal.shared.CreditCard;
import com.journal.shared.Like;
import com.journal.shared.User;

public class DatabaseReferences {
	private static DB instance = null;

	synchronized public static DB getInstance() { 
		if(instance  == null) {
			instance = DBMaker.newDirectMemoryDB().make();
			initializeDb();
		}
		return instance;
	}

	//Metodo di inizializzazione db
	@SuppressWarnings("deprecation")
	private static void initializeDb() {


		User user = new User("user", "user","user", "user", "user","M",new Date(97,8,14),"Morciano","Via XXV Settembre");
		user.setPremium(true);	
		CreditCard creditCard = new CreditCard("5555444433332222","user","user",new Date(21,10,10),123,"Italy",47842);
		user.setCreditCard(creditCard);
		createUser(user);

		//Utente non Premium
		createUser(new User("aaaaa", "aaaaa","aaaaa", "aaaaa", "aaaaa","M",new Date(97,8,14),"Morciano","Via XXV Settembre"));


		//Admin
		User admin = new User("admin", "admin","admin", "admin", "admin","M",new Date(97,8,14), "Morciano","Via Roma");
		admin.setAdmin(true);
		createUser(admin);


		Article articlePremium = new Article("Decreto sostegno","ristori dal 20 al 60 per cento","user", "politica estera",true);
		Article articleNonPremium = new Article("Decreto sostegno","ristori dal 20 al 60 per cento","user", "sport",false);

		createArticle(articlePremium);
		createArticle(articleNonPremium);

		Article articlePremium1 = new Article("Premium","premium","user", "premium",true);
		Article articleNonPremium1 = new Article("1","1",new Date(21,3,27),"user", "politica estera",false);
		Article articleNonPremium2 = new Article("2","2",new Date(21,3,28),"user", "sport",false);
		Article articleNonPremium3 = new Article("3","3",new Date(21,3,29),"user", "politica estera",false);
		Article articleNonPremium4 = new Article("4","4",new Date(21,3,30),"user", "sport",false);
		Article articleNonPremium5 = new Article("8","8",new Date(21,3,26),"user", "politica estera",false);
		Article articleNonPremium6 = new Article("7","7",new Date(21,3,25),"user", "sport",false);
		Article articleNonPremium7 = new Article("6","6",new Date(21,3,24),"user", "politica estera",false);
		Article articleNonPremium8 = new Article("5","5",new Date(21,3,23),"user", "sport",false);

		createArticle(articlePremium1);
		createArticle(articleNonPremium1);
		createArticle(articleNonPremium2);
		createArticle(articleNonPremium3);
		createArticle(articleNonPremium4);
		createArticle(articleNonPremium5);
		createArticle(articleNonPremium6);
		createArticle(articleNonPremium7);
		createArticle(articleNonPremium8);

		createRootCategory();
		createCategory("Italia", "root");
		createCategory("Mondo", "root");
		createCategory("Spettacolo", "root");
		createCategory("Sport", "root");
		createCategory("Tecnologia", "root");
		createCategory("Scienza", "root");
		createCategory("Politica", "root");
		createCategory("Business", "root");
		createCategory("Salute", "root");
		createCategory("Arte", "root");
	}

	/* ------------------------------ USERS ------------------------------ */

	//Metodo di creazione utente
	public static boolean createUser(User user) {
		user.setId(UUID.randomUUID().toString());

		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");

		//per tutti gli user cerca un match per il risultato della funzione che gli passo
		boolean existing = users.stream().anyMatch(currentUser -> Objects.equals(currentUser.getEmail(),user.getEmail()));
		if(existing) {
			return false;
		} 
		users.add(user);
		db.commit();

		return true;
	}

	//GET
	public static User getUser(String email, String password) {
		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");

		return users.stream()
				.filter(user -> Objects.equals(user.getEmail(), email) && Objects.equals(user.getPassword(), password))
				.findFirst()
				.orElse(null);


	}

	public static User getUserById(String id) {
		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");

		return users.stream()
				.filter(user -> Objects.equals(user.getId(), id))
				.findFirst()
				.orElse(null);

	}

	public static User getUserByUsername(String username) {
		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");

		return users.stream()
				.filter(user -> Objects.equals(user.getUsername(), username))
				.findFirst()
				.orElse(null);
	}

	/* ------------------------------ ARTICLE ------------------------------ */

	//Metodo di creazione articolo
	public static boolean createArticle(Article article) {
		article.setId(UUID.randomUUID().toString());

		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");
		articles.add(article);


		Set<User> users = db.getHashSet("users");
		User user = users.stream()
				.filter(currentUser-> Objects.equals(currentUser.getUsername(), article.getAuthor()))
				.findFirst()
				.orElse(null);

		user.getWrittenArticles().add(article);
		db.commit();
		return true;
	}

	//Metodo di update articolo
	public static boolean updateArticle(Article article) {
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");

		Article existingArticle = articles.stream()
				.filter(currentArticle -> Objects.equals(currentArticle.getId(), article.getId()))
				.findFirst()
				.orElse(null);

		if(Objects.isNull(existingArticle)) {
			return false;
		}

		existingArticle.setBody(article.getBody());
		existingArticle.setPremium(article.isPremium());
		existingArticle.setTitle(article.getTitle());
		existingArticle.setCategoryName(article.getCategoryName());

		db.commit();

		return true;
	}

	//Metodo di eliminazione articolo
	public static boolean deleteArticle(String id) {
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");

		boolean result = articles.removeIf(article->Objects.equals(article.getId(), id));

		db.commit();

		return result;
	}

	//Metodi GETTER
	public static Set<Article> getArticles(){
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");
		Set<Article> articlesMapping = new HashSet<>();
		articles.forEach(article -> articlesMapping.add(article));

		return articlesMapping;
	}

	public static Article getArticleById(String id) {
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");

		return articles.stream()
				.filter(article -> Objects.equals(article.getId(), id))
				.findFirst()
				.orElse(null);
	}

	public static Set<Article> getLikedArticles(String idUser){
		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");
		User user = users.stream()
				.filter(currenrUser -> Objects.equals(currenrUser.getId(), idUser))
				.findFirst()
				.orElse(null);

		Set<Article> articlesMapping = new HashSet<>();
		user.getLikedArticles().forEach(article -> articlesMapping.add(article));
		return articlesMapping;
	}

	public static Set<Article> getCommentedArticles(String idUser){
		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");
		User user = users.stream()
				.filter(currenrUser -> Objects.equals(currenrUser.getId(), idUser))
				.findFirst()
				.orElse(null);

		Set<Article> articlesMapping = new HashSet<>();
		user.getCommentedArticles().forEach(article -> articlesMapping.add(article));
		return articlesMapping;
	}

	public static Set<Article> getWrittenArticles(String idUser) {
		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");
		User user = users.stream()
				.filter(currenrUser -> Objects.equals(currenrUser.getId(), idUser))
				.findFirst()
				.orElse(null);

		Set<Article> articlesMapping = new HashSet<>();
		user.getWrittenArticles().forEach(article -> articlesMapping.add(article));
		return articlesMapping;
	}

	/* ------------------------------ CREDITCARD ------------------------------ */

	//Metodo di creazione CreditCard
	public static boolean createCreditCard(CreditCard creditcard, String id) {
		DB db = DatabaseReferences.getInstance();
		Set<User> users = db.getHashSet("users");

		User user = users.stream()
				.filter(currentUser-> Objects.equals(currentUser.getId(), id))
				.findFirst()
				.orElse(null);

		if(Objects.isNull(user)) {
			return false;
		}

		user.setCreditCard(creditcard);
		user.setPremium(true);
		db.commit();

		return true;
	}

	/* ------------------------------ LIKE ------------------------------ */

	//Metodo di creazione Like
	public static boolean createLike(String idArticle, String idUser) {
		Like like = new Like(idArticle, idUser);

		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");

		Article article = articles.stream()
				.filter(currentArticle -> Objects.equals(currentArticle.getId(), idArticle))
				.findFirst()
				.orElse(null);
		if(Objects.isNull(article)) {
			return false;
		}

		article.getLikes().add(like);

		Set<User> users = db.getHashSet("users");
		User user = users.stream()
				.filter(currentUser -> Objects.equals(currentUser.getId(), idUser))
				.findFirst()
				.orElse(null);
		if(Objects.isNull(user)) {
			return false;
		}

		System.out.println(article.toString());
		user.getLikedArticles().add(article);

		System.out.println("addado alla lista");
		db.commit();		
		return true;

	}

	//Metodo di rimozione Like
	public static boolean removeLike(String idArticle, String idUser) {
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");
		Article article = articles.stream()
				.filter(currentArticle -> Objects.equals(currentArticle.getId(), idArticle))
				.findFirst()
				.orElse(null);

		if(Objects.isNull(article)) {
			return false;
		}

		boolean result = article.getLikes().removeIf(currentLike -> currentLike.getIdUser().equals(idUser));
		Set<User> users = db.getHashSet("users");
		User user = users.stream()
				.filter(currentUser -> Objects.equals(currentUser.getId(), idUser))
				.findFirst()
				.orElse(null);

		if(Objects.isNull(user)) {
			return false;
		}

		boolean result2 = user.getLikedArticles().removeIf(currentArticle -> currentArticle.getId().equals(idArticle));
		db.commit();

		if(result==result2 && result2==true) {
			return true;
		}

		if(result==result2 && result2==false) {
			return false;
		}

		if(result!=result2) {
			return false;
		}
		return false;

	}

	/* ------------------------------ COMMENT ------------------------------ */

	//Metodo di creazione commento da parte di utente amministratore
	public static boolean createComment(String articleID, Comment comment) {
		comment.setId(UUID.randomUUID().toString());
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");

		Article article = articles.stream()
				.filter(currentArticle -> Objects.equals(currentArticle.getId(), articleID))
				.findFirst()
				.orElse(null);
		if(Objects.isNull(article)) {
			return false;
		}
		article.getComments().add(comment);
		Set<User> users = db.getHashSet("users");
		User user = users.stream()
				.filter(currentUser -> Objects.equals(currentUser.getEmail(), comment.getEmail()))
				.findFirst()
				.orElse(null);
		if(Objects.isNull(user)) {
			return false;
		}

		user.getCommentedArticles().add(article);

		db.commit();
		return true;

	}

	//Metodo di approvazione commento da parte di utente amministratore
	public static boolean approveComment(String idArticle, String idComment) {
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");

		Article article = articles.stream()
				.filter(currentArticle -> Objects.equals(currentArticle.getId(), idArticle))
				.findFirst()
				.orElse(null);

		if(Objects.isNull(article)) {
			return false;
		}

		Comment comment = article.getComments().stream()
				.filter(currentComment-> Objects.equals(currentComment.getId(), idComment))
				.findFirst()
				.orElse(null);

		if(Objects.isNull(comment)) {
			return false;
		}

		comment.setApproved(true);
		db.commit();

		return true;
	}

	//Metodo di eliminazione commento da parte di utente amministratore
	public static boolean deleteComment(String idArticle, String idComment) {
		DB db = DatabaseReferences.getInstance();
		Set<Article> articles = db.getHashSet("articles");

		Article article = articles.stream()
				.filter(currentArticle -> Objects.equals(currentArticle.getId(), idArticle))
				.findFirst()
				.orElse(null);

		if(Objects.isNull(article)) {
			return false;
		}

		boolean result = article.getComments().removeIf(currentComment -> Objects.equals(currentComment.getId(), idComment));

		db.commit();

		return result;
	}

	/* ------------------------------ CATEGORY ------------------------------ */

	//Metodo di creazione categoria da parte di utente amministratore
	public static boolean createCategory(String name, String parent) {
		DB db = DatabaseReferences.getInstance();
		Category rootCategory = (Category) db.getHashMap("categories").get("root");
		Category parentCategory = findCategoryByName(rootCategory, parent);

		if(Objects.isNull(parentCategory)) {
			return false;
		}

		Category childCategory = new Category(name, parentCategory);
		parentCategory.getChildren().add(childCategory);
		db.commit();
		return true;
	}

	//Metodo di eliminazione categoria da parte di utente amministratore
	public static Boolean deleteCategory(String name) {
		DB db = DatabaseReferences.getInstance();
		Category rootCategory = (Category) db.getHashMap("categories").get("root");
		Category categoryToDelete = findCategoryByName(rootCategory, name);

		if(Objects.isNull(categoryToDelete)) {
			return null;
		}

		boolean result = categoryToDelete.getParent().getChildren().removeIf(child -> Objects.equals(child.getName(), name));
		db.commit();
		return result;
	}

	//Metodo di creazione categoria ROOT
	public static void createRootCategory() {
		DB db = DatabaseReferences.getInstance();
		Map<String, Category> categories = db.getHashMap("categories");
		categories.put("root", new Category("root",null));
		db.commit();
	}

	//Metodi GETTER
	public static Category findCategoryByName(Category category, String name) {
		if(Objects.equals(category.getName(), name)) {
			return category;
		}
		for(Category child:category.getChildren()) {
			Category categoryToFind = findCategoryByName(child, name);
			if(Objects.nonNull(categoryToFind)) {
				return categoryToFind;
			}
		}
		return null;
	}

	public static Category getCategoryByName(String name) {

		Category root = getCategoryRoot();
		return findCategoryByName(root, name);
	}

	public static Category getCategoryRoot() {
		DB db = DatabaseReferences.getInstance();
		Category category = (Category) db.getHashMap("categories").get("root");
		return category;
	}

	public static Set<Category> getCategories() {
		DB db = DatabaseReferences.getInstance();
		Category rootCategory = (Category) db.getHashMap("categories").get("root");
		Set<Category> categories = new HashSet<Category>();

		setAllCategories(rootCategory, categories);

		return categories;
	}

	//Metodi SETTER
	public static Category updateCategory(String oldName, String newName) {
		DB db = DatabaseReferences.getInstance();

		Category rootCategory = (Category) db.getHashMap("categories").get("root");
		Category categoryToUpdate = findCategoryByName(rootCategory, oldName);

		if (Objects.nonNull(categoryToUpdate)) {
			categoryToUpdate.setName(newName);

			Set<Article> articles = db.getHashSet("articles");
			articles.stream()
			.filter(article -> Objects.equals(article.getCategoryName(), oldName))
			.forEach(article -> article.setCategoryName(newName));			

			db.commit();
		}			

		return categoryToUpdate;		
	}

	private static void setAllCategories(Category parentCategory, Set<Category> categories) {
		categories.add(parentCategory);
		parentCategory.getChildren().forEach(childCategory -> setAllCategories(childCategory, categories));
	}

}
