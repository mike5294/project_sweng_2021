package com.journal.server;


import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.journal.client.LikeService;
import com.journal.shared.Article;

public class LikeServiceImpl extends RemoteServiceServlet implements LikeService{
	
	private static final long serialVersionUID = 1L;

	@Override
	public Article createLike(String idArticle, String idUser) {		
		DatabaseReferences.createLike(idArticle, idUser);
		return DatabaseReferences.getArticleById(idArticle);
	}

	@Override
	public Article removeLike(String idArticle, String idUser) {
		DatabaseReferences.removeLike(idArticle, idUser);
		return DatabaseReferences.getArticleById(idArticle);
	}
	
	
	
	

}
