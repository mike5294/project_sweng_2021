package com.journal.server;


import java.util.Date;
import java.util.Set;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.journal.client.UserService;
import com.journal.shared.Article;
import com.journal.shared.CreditCard;
import com.journal.shared.User;

public class UserServiceImpl extends RemoteServiceServlet implements UserService{

	private static final long serialVersionUID = 1L;

	@Override
	public User createUser(String email, String password, String name, String surname, String username, String sex, Date birthdate, String birthplace, String address) {
		User user = new User(email, password, name, surname, username, sex, birthdate, birthplace, address);
		boolean result = DatabaseReferences.createUser(user);
		return result?user:null;
	}

	@Override
	public User getUser(String email, String password) {
		return DatabaseReferences.getUser(email, password);
	}
	
	@Override
	public User getUserByUsername(String username) {
		return DatabaseReferences.getUserByUsername(username);
	}

	@Override
	public Set<Article> getLikedArticles(String id) {
		return DatabaseReferences.getLikedArticles(id);
	}
	@Override
	public Set<Article> getCommentedArticles(String id) {
		return DatabaseReferences.getCommentedArticles(id);
	}
	
	@Override
	public Set<Article> getWrittenArticles(String id) {
		return DatabaseReferences.getWrittenArticles(id);
	}
	
	@Override
	public CreditCard createCreditCard(CreditCard creditcard, String id) {
		boolean result = DatabaseReferences.createCreditCard(creditcard, id);
		return result?creditcard:null;
	}

	
	

}
