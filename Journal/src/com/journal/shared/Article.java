package com.journal.shared;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Article implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String title;
	private String body;
	private Date publicationDate;
	private String author;
	private String categoryName;
	private boolean premium;
	private List<Like> likes;
	private List<Comment> comments;
	
	public Article() {}

	public Article(String title, String body, String author, String categoryName, boolean premium) {
		this.title = title;
		this.body = body;
		this.publicationDate = new Date();
		this.author = author;
		this.categoryName = categoryName;
		this.premium = premium;
		this.likes = new ArrayList<>();
		this.comments = new ArrayList<>();
	}
	
	
	public Article(String title, String body, Date publicationDate,String author, String categoryName, boolean premium) {
		this.title = title;
		this.body = body;
		this.publicationDate = publicationDate;
		this.author = author;
		this.categoryName = categoryName;
		this.premium = premium;
		this.likes = new ArrayList<>();
		this.comments = new ArrayList<>();
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public boolean isPremium() {
		return premium;
	}

	public void setPremium(boolean premium) {
		this.premium = premium;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Like> getLikes() {
		return likes;
	}

	public void setLikes(List<Like> likes) {
		this.likes = likes;
	}
	
	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", body=" + body + ", publicationDate=" + publicationDate
				+ ", author=" + author + ", premium=" + premium + ", likes=" + likes + ", comments=" + comments + "]";
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * 
	 */
	
}
