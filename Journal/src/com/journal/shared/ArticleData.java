package com.journal.shared;
import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;

public class ArticleData extends JavaScriptObject{
	
	protected ArticleData() {}
	
	public final native String getId() /*-{ return this.id; }-*/;
    public final native String getTitle() /*-{ return this.title; }-*/;
    public final native String getBody() /*-{ return this.body; }-*/;
    public final native Date getPublicationDate() /*-{ return this.publicationDate; }-*/;
    public final native String getAuthor() /*-{ return this.author; }-*/;
    public final native String getCategoryName() /*-{ return this.categoryName; }-*/;
    public final native boolean isPremium() /*-{ return this.premium; }-*/;
    public final native List<Like> getLikes() /*-{ return this.likes; }-*/;
	public final native List<Comment> getComments() /*-{ return this.comments; }-*/;
}
