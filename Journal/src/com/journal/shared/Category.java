package com.journal.shared;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Category implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private Category parent; 
	private List<Category> children;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Category getParent() {
		return parent;
	}
	public void setParent(Category parent) {
		this.parent = parent;
	}
	public List<Category> getChildren() {
		return children;
	}
	public void setChildren(List<Category> children) {
		this.children = children;
	}
	
	public Category() {}
	
	public Category(String name, Category parent) {
		this.name = name;
		this.parent = parent;
		this.children = new ArrayList<Category>();
	}
}
