package com.journal.shared;

import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;

public class CategoryData extends JavaScriptObject{
	protected CategoryData() {}
	
	public final native String getName() /*-{ return this.name; }-*/;
    public final native Category getParent() /*-{ return this.parent; }-*/;
    public final native List<Category> getChildren() /*-{ return this.children; }-*/;
}
