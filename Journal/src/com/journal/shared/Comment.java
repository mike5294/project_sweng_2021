package com.journal.shared;
import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String email;
	private String text;
	private boolean approved;
	private Date date;
	public String getId() {
		return id;
	}
	
	public Comment(){
		
	}
	
	public Comment(String email, String text, boolean approved, Date date) {
		this.email = email;
		this.text = text;
		this.approved = approved;
		this.date = date;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isApproved() {
		return approved;
	}
	public void setApproved(boolean approved) {
		this.approved = approved;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
