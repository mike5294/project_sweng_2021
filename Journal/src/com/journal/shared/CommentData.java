package com.journal.shared;
import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;

public class CommentData extends JavaScriptObject{
	
	protected CommentData() {}
	
	public final native String getId() /*-{ return this.id; }-*/;
    public final native String getEmail() /*-{ return this.email; }-*/;
    public final native String getText() /*-{ return this.text; }-*/;
    public final native boolean isApproved() /*-{ return this.approved; }-*/;
    public final native Date getDate() /*-{ return this.date; }-*/;
}
