package com.journal.shared;
import java.io.Serializable;
import java.util.Date;

public class CreditCard implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String number;
	private String name;
	private String surname;
	private Date expirationDate;
	private int cvv;
	private String nation;
	private int cap;
	
	public CreditCard() {}
	
	public CreditCard(String number, String name, String surname, Date expirationDate, int cvv, String nation, int cap) {
		this.number = number;
		this.name = name;
		this.surname = surname;
		this.expirationDate = new Date();
		this.cvv = cvv;
		this.nation = nation;
		this.cap = cap;
	}

	

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public int getCvv() {
		return cvv;
	}

	public void setCvv(int cvv) {
		this.cvv = cvv;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public int getCap() {
		return cap;
	}

	public void setCap(int cap) {
		this.cap = cap;
	}

	@Override
	public String toString() {
		return "CreditCard [number=" + number + ", name=" + name + ", surname=" + surname + ", expirationDate="
				+ expirationDate + ", cvv=" + cvv + ", nation=" + nation + ", cap=" + cap + "]";
	}
	
	
	
	
}
