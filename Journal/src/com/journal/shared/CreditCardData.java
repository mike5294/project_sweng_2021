package com.journal.shared;

import java.util.Date;

import com.google.gwt.core.client.JavaScriptObject;

public class CreditCardData extends JavaScriptObject {
	
	protected CreditCardData() {}
	
	public final native String getNumber() /*-{ return this.number; }-*/;
    public final native String getName() /*-{ return this.name; }-*/;
    public final native String getSurname() /*-{ return this.surname; }-*/;
    public final native Date getExpirationDate() /*-{ return this.expirationDate; }-*/;
    public final native int getCvv() /*-{ return this.cvv; }-*/;
    public final native String getNation() /*-{ return this.nation; }-*/;
    public final native int getCap() /*-{ return this.cap; }-*/;
}
