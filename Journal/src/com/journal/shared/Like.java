package com.journal.shared;

import java.io.Serializable;

public class Like implements Serializable {
	@Override
	public String toString() {
		return "Like [idArticle=" + idArticle + ", idUser=" + idUser + "]";
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idArticle;
	private String idUser;
	
	public Like() {}
	
	public Like(String idArticle, String idUser) {
		this.idArticle = idArticle;
		this.idUser = idUser;
	}
	
	public String getIdArticle() {
		return idArticle;
	}
	public void setIdArticle(String idArticle) {
		this.idArticle = idArticle;
	}
	
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
}
