package com.journal.shared;

import com.google.gwt.core.client.JavaScriptObject;

public class LikeData extends JavaScriptObject{
	
	protected LikeData() {}
	
	public final native String getIdArticle() /*-{ return this.idArticle; }-*/;
    public final native String getIdUser() /*-{ return this.idUser; }-*/;
}
