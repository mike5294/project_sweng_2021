package com.journal.shared;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User implements Serializable {
	/*private String name;
	private String surname;
	
	private Date birthdate;
	private String email;
	private String address;
	
	private String birthplace;
	private String sex;
	private List<Category> categories;
	private CreditCard creditCard;*/
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String email;
	private String password;
	private String name;
	private String surname;
	private String username;
	private String sex;
	private Date birthdate;
	private Date registrationDate;
	private String birthplace;
	private String address;
	private boolean isPremium;
	private boolean isAdmin;

	private CreditCard creditCard;
	private List<Article> likedArticles;
	private List<Article> commentedArticles;
	private List<Article> writtenArticles;

	public User() {}
	
	//mi serve solo per l'utente iniziale autocreato USER USER
	public User(String email, String password) {
		this.email = email;
		this.password = password;
		this.isPremium = false;
		this.isAdmin = false;
		this.registrationDate = new Date();
		this.creditCard  = new CreditCard();
		this.likedArticles = new ArrayList<Article>();
		this.commentedArticles = new ArrayList<Article>();
		this.writtenArticles = new ArrayList<Article>();
		}	
	
	public User(String email, String password, String name, String surname, String username, String sex, Date birthdate, String birthplace, String address) {
		this.email = email;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.username = username;
		this.sex = sex;
		this.birthdate = birthdate;
		this.registrationDate =  new Date();
		this.birthplace = birthplace;
		this.address = address;
		this.isPremium = false;
		this.isAdmin = false;
		this.creditCard  = new CreditCard();
		this.likedArticles = new ArrayList<Article>();
		this.commentedArticles = new ArrayList<Article>();
		this.writtenArticles = new ArrayList<Article>();
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public String getBirthplace() {
		return birthplace;
	}

	public void setBirthplace(String birthplace) {
		this.birthplace = birthplace;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isPremium() {
		return isPremium;
	}

	public void setPremium(boolean isPremium) {
		this.isPremium = isPremium;
	}
	
	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public List<Article> getLikedArticles() {
		return likedArticles;
	}

	public void setLikedArticles(List<Article> likedArticles) {
		this.likedArticles = likedArticles;
	}
	public List<Article> getCommentedArticles() {
		return commentedArticles;
	}

	public void setCommentedArticles(List<Article> commentedArticles) {
		this.commentedArticles = commentedArticles;
	}

	public List<Article> getWrittenArticles() {
		return writtenArticles;
	}

	public void setWrittenArticles(List<Article> writtenArticles) {
		this.writtenArticles = writtenArticles;
	}
	
		
	
}
