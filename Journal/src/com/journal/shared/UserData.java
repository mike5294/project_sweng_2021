package com.journal.shared;

import java.util.Date;
import java.util.List;

import com.google.gwt.core.client.JavaScriptObject;

public class UserData extends JavaScriptObject{
	
	protected UserData() {}

    public final native String getEmail() /*-{ return this.email; }-*/;
    public final native String getPassword() /*-{ return this.password; }-*/;
    public final native String getName() /*-{ return this.name; }-*/;
    public final native String getSurname() /*-{ return this.surname; }-*/;
    public final native String getUsername() /*-{ return this.username; }-*/;
    public final native String getSex() /*-{ return this.sex; }-*/;
    public final native Date getBirthdate() /*-{ return this.birthdate; }-*/;
    public final native Date getRegistrationDate() /*-{ return this.registrationDate; }-*/;
    public final native String getBirthplace() /*-{ return this.birthplace; }-*/;
    public final native String getAddress() /*-{ return this.address; }-*/;
    public final native boolean isPremium() /*-{ return this.isPremium; }-*/;
    public final native boolean isAdmin() /*-{ return this.isAdmin; }-*/;
    public final native CreditCard getCreditCard() /*-{ return this.creditCard; }-*/;
    public final native List<Article> getLikedArticles() /*-{ return this.likedArticles; }-*/;
    public final native List<Article> getCommentedArticles() /*-{ return this.commentedArticles; }-*/;
    public final native List<Article> getWrittenArticles() /*-{ return this.writtenArticles; }-*/;
}
