package com.journal.test;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.Objects;

import org.junit.BeforeClass;
import org.junit.Test;

import com.journal.server.ArticleServiceImpl;
import com.journal.server.CategoryServiceImpl;
import com.journal.server.CommentServiceImpl;
import com.journal.server.DatabaseReferences;
import com.journal.server.LikeServiceImpl;
import com.journal.server.UserServiceImpl;
import com.journal.shared.Article;
import com.journal.shared.Category;
import com.journal.shared.Comment;
import com.journal.shared.CreditCard;
import com.journal.shared.User;

public class JUnitTest {
	private static UserServiceImpl userService = new UserServiceImpl();
	private static ArticleServiceImpl articleService = new ArticleServiceImpl();
	private static CategoryServiceImpl categoryService = new CategoryServiceImpl();
	private static CommentServiceImpl commentService = new CommentServiceImpl();
	private static LikeServiceImpl likeService = new LikeServiceImpl();
	private static User user;
	private static Article article;
	private static Comment comment;
	private static CreditCard creditCard;
	private static Category category;
	
	@BeforeClass
	public static void beforeTest() {
		DatabaseReferences.createRootCategory();
		
		user = userService.createUser("test", "test", "test", "test", "test", "M", new Date(97/8/14), "Cattolica", "via del test");
		categoryService.createCategory("politica", "root");
		category = categoryService.getCategoryByName("politica");
		article = articleService.createArticle("title", "body", "test", "politica", false);
		commentService.createComment(article.getId(), user.getEmail(), "commento");	
		comment = article.getComments().get(0);
		creditCard = new CreditCard("5555444433332222", "test", "test", new Date(21/8/14), 123, "Italy", 47842);
		userService.createCreditCard(creditCard, user.getId());
	}
	
	@Test
	public void testCreateUser() {
		assertTrue(Objects.nonNull(userService.getUser("test", "test")));		
	}
	
	@Test
	public void testCreateCreditCard() {
		assertTrue(Objects.nonNull(userService.getUser("test", "test").getCreditCard()));
	}
	
	@Test
	public void testCreateArticle() {
		assertTrue(Objects.nonNull(articleService.getArticleById(article.getId())));
	}
	
	@Test
	public void testUpdateArticle() {
		articleService.updateArticle(article.getId(), "body modificato", false, "title modificato", "politica");
		assertTrue(article.getBody().equalsIgnoreCase("body modificato"));
	}
	
	@Test
	public void testDeleteArticle() {
		assertTrue(articleService.deleteArticle(article.getId()));
	}
	
	@Test
	public void testCreateCategory() {
		assertTrue(Objects.nonNull(categoryService.getCategoryByName(category.getName())));
	}
	
	@Test
	public void testUpdateCategory() {
		categoryService.updateCategory(category.getName(), "politica modificata");
		assertTrue(category.getName().equalsIgnoreCase("politica modificata"));
	}
	
	@Test
	public void testDeleteCategory() {
		assertTrue(categoryService.deleteCategory(category.getName()));
	}
	
	public void testCreateComment() {
		assertTrue(Objects.nonNull(article.getComments().get(0)));
	}
	
	public void testApproveComment() {
		commentService.approveComment(article.getId(), comment.getId());
		assertTrue(comment.isApproved());
	}
	
	public void testDeleteComment() {
		commentService.deleteComment(article.getId(), comment.getId());
		assertTrue(Objects.isNull(article.getComments().get(0)));
	}
	
	public void testCreateLike() {
		likeService.createLike(article.getId(), user.getId());
		assertTrue(Objects.nonNull(article.getLikes()));
	}
	
	public void testRemoveLike() {
		assertTrue(Objects.isNull(article.getLikes()));
	}		
}
